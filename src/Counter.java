public class Counter implements Runnable {

    private StopWatch stopWatch;
    private String name;

    public Counter(StopWatch stopWatch, String name) {
        this.stopWatch = stopWatch;
        this.name = name;
    }

    @Override
    public void run() {
        while(true){
            try {

                synchronized(stopWatch) {
                    StopWatch.count();
                    System.out.println(" Time: "+StopWatch.getTime());
                    Thread.sleep(1000);
                }
            } catch (Exception ex){
                System.out.println(this.name + " interrupted!");
            }

        }
    }

}
