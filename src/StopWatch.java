public class StopWatch {
    private String name;
    private volatile static int time= 0;

    public StopWatch(String name){
        this.name = name;


    }

    public static synchronized void count(){
        time++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static synchronized long getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
